import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  bodyText: {
    fontFamily: 'open-sans',
  },
  title: {
    // fontFamily: 'open-sans-bold', // BUG: You started loading the font "open-sans-bold", but used it before it finished loading. - You need to wait for Font.loadAsync to complete before using the font. - We recommend loading all fonts before rendering the app, and rendering only Expo.AppLoading while waiting for loading to complete.
    fontSize: 18,
  },
});
